import React, { Component } from 'react'
import { BrowserRouter as Router, Route,Switch } from 'react-router-dom'
import HomeContainer from './containers/HomeContainer'
import PostsContainer from './containers/PostsContainer'
import PostContainer from './containers/PostContainer'
import newPostContainer from './containers/newPostContainer'

class App extends Component {
  static propTypes = {
    
  }

  render() {
    return (
      <Router>
        <div className="App">
          <Route exact path="/" component={HomeContainer} />
          <Route exact path="/posts" component={PostsContainer} />
          <Route path="/posts/new" component={newPostContainer}/>
          <Switch>
            <Route path="/posts/:post_id/respuestas" component={props=><PostContainer postId={props.match.params.post_id} ></PostContainer>} />
          </Switch>
        </div>
      </Router>
    )
  }
}

export default App
