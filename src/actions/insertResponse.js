import { createAction } from 'redux-actions'
import { INSERT_RESPONSE } from '../constans';
import { apiPost } from '../api';
import { urlResponses } from '../api/urls';

export const insertResponse = createAction(INSERT_RESPONSE,
    response => apiPost(urlResponses,response)() )