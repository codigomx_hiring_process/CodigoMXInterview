import { createAction } from 'redux-actions'
import { INSERT_POST } from '../constans';
import { apiPost } from '../api';
import { urlPosts } from '../api/urls';

export const insertPost = createAction(INSERT_POST,
    post => apiPost(urlPosts,post)() )