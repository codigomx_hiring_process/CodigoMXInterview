import {createAction } from 'redux-actions'
import { FETCH_RESPONSES } from '../constans'
import { apiGet } from '../api';
import { urlResponses } from '../api/urls';

export const fetchResponses = createAction(FETCH_RESPONSES,
    post_id => apiGet(urlResponses,{post_id})() )