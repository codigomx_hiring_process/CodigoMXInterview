import React,{Component}from 'react'
import { withRouter } from 'react-router-dom'
import AppFrame from '../components/AppFrame'
import PostsActions from '../components/PostsActions'

class HomeContainer extends Component {
    handleOnClick = () => {
        this.props.history.push('/posts')
    }
    render (){
        return (
            <div>
                <AppFrame 
                    header="Home"
                    body={
                        <div>
                            Esta es la pantalla principal
                            <PostsActions>
                                <button onClick={this.handleOnClick}>Listado de posts</button>
                            </PostsActions>    
                        </div>
                    } 
                />
            </div>
        )
    }
};

HomeContainer.propTypes = {
    
};

export default withRouter(HomeContainer);