import React,{Component} from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import AppFrame from '../components/AppFrame';
import PostsList from '../components/PostsList';
import PostsActions from '../components/PostsActions'
import { fetchPosts } from '../actions/fetchPosts';
import { getPosts } from '../selectors/posts';

class PostsContainer extends Component {

    componentDidMount() {
        if(this.props.posts.length === 0){
            this.props.fetchPosts()
        }
    }
    

    handleAddNew = ()=>{
        this.props.history.push("/posts/new")
    }

    renderBody = posts => (
                        <div>
                            <PostsActions>
                                <button onClick={this.handleAddNew} >Crear nuevo posts</button>
                            </PostsActions>
                            <PostsList posts={posts} />
                        </div>
                        )
    

    render(){
        return (
            <div>
                <AppFrame 
                        header="Listado de posts"
                        body={this.renderBody(this.props.posts)} 
                    />
            </div>
        );
    }

};

PostsContainer.propTypes = {
    fetchPosts:PropTypes.func.isRequired,
    posts:PropTypes.array.isRequired,
}

PostsContainer.defaultProps = {
    posts:[]
}

const mapStatesToProps = state => ({
    posts:getPosts(state)
})

export default withRouter(connect(mapStatesToProps,{ fetchPosts })(PostsContainer));