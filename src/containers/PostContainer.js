import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom' 
import AppFrame from './../components/AppFrame'
import { getPostById } from '../selectors/posts'
import { fetchResponses } from './../actions/fetchResponses'
import { getResponses } from '../selectors/responses'
import ResponsesList from '../components/ResponsesList'
import NewResponse from '../components/NewResponse'
import { insertResponse } from './../actions/insertResponse'
import { fetchPosts } from './../actions/fetchPosts'

class PostContainer extends Component {

    componentDidMount() {
      if(!this.props.post){
          this.props.fetchPosts()
      }

      this.props.fetchResponses(this.props.postId)
    }

    handleSubmit = values=>{
        const data = {...values,post_id:this.props.postId}
        this.props.insertResponse(data)
    }

    handleOnBack = ()=>{
        this.props.history.goBack()
    }

    handleOnSubmitSuccess = ()=>{

    }

    
    renderBody = responses => (
        <div>
            <p>{this.props.post && this.props.post.post_content}</p>
            <hr/>
            <ResponsesList responses={responses} />
            <NewResponse 
                onSubmit={this.handleSubmit}
                onSubmitSuccess={this.handleOnSubmitSuccess}
                onBack={this.handleOnBack}
            />
        </div>
    )

    render() {
        return (
            <div>
                <AppFrame header={`Respuestas del post ${this.props.postId}`}
                    body={this.renderBody(this.props.responses)}
                >
                </AppFrame>
            </div>
        )
    }
}

PostContainer.propTypes = {
    post:PropTypes.object,
    postId:PropTypes.string.isRequired,
    responses:PropTypes.array.isRequired,
    fetchResponses:PropTypes.func.isRequired,
    insertResponse:PropTypes.func.isRequired
}

PostContainer.defaultProps = {
    responses:[]
}

const mapStateToProps = (state,props) => {
    return (
        {
            post:       getPostById(state,props),
            responses:  getResponses(state)
        }
    )
}

export default withRouter(connect(mapStateToProps,{ fetchResponses,insertResponse,fetchPosts })(PostContainer))