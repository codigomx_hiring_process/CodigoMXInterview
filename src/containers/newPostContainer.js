import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { withRouter } from 'react-router-dom' 
import {SubmissionError} from 'redux-form'
import AppFrame from '../components/AppFrame'
import NewPost from '../components/NewPost'
import { insertPost } from './../actions/insertPost'

class newPostContainer extends Component {
    static propTypes = {
        insertPost:PropTypes.func.isRequired    
    }

    handleSubmit = values => {
        return this.props.insertPost(values).then( res=> {
            if(res.error){
                throw new SubmissionError(res.payload)
            }
        })
    }
    handleOnSubmitSuccess = () => {
        this.props.history.goBack()
    }
    handleOnBack = () => {
        this.props.history.goBack()
    }

    renderBody = () =>(<NewPost 
            onSubmit={this.handleSubmit}
            onSubmitSuccess={this.handleOnSubmitSuccess}
            onBack={this.handleOnBack} />)

    render() {
        return (
            <div>
                <AppFrame header="Nuevo post"
                    body={this.renderBody()}
                >
                </AppFrame>
            </div>
        )
    }
}

export default withRouter(connect(null,{ insertPost })(newPostContainer))