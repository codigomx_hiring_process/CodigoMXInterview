import React from 'react';
import PropTypes from 'prop-types';
import ResponseListItem from './ResponseListItem';

const ResponsesList = ({ responses }) => {
    return (
        <div>
            <div className="response-list">
                {
                    responses.map(response=>
                        (<ResponseListItem
                            key={response.response_id}
                            response_content={response.response_content}
                            created_at={response.created_at}
                        />)
                    )
                }
            </div>
        </div>
    );
};

ResponsesList.propTypes = {
    responses:PropTypes.array.isRequired,
};

export default ResponsesList;