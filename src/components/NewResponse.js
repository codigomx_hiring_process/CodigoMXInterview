import React from 'react'
import { reduxForm, Field } from 'redux-form'

const textAreaField = ({input,meta,label,name})=>(
    <div>
        <label htmlFor={name}>{label}</label>
        <br/>
        <input {...input} />
        <br/>
            {
                meta.touched && meta.error && <span> {meta.error} </span>
            }
    </div>
)

const isRequired = value => !value && "Este campo es requerido"

const NewResponse = ({handleSubmit,submitting}) => {
    return (
        <div>
            <form onSubmit={handleSubmit}>
                <Field label="Contenido de la respuesta" name="response_content" component={textAreaField} type="text" validate={isRequired}></Field>
                <button type="submit" disabled={submitting} >Guardar</button>
            </form>
        </div>
    );
};



export default reduxForm({ form:"newResponse" })(NewResponse);