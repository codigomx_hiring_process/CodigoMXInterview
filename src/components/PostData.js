import React from 'react'
import PropTypes from 'prop-types'

const PostData = ({content_post}) => {
  return (
    <div>
        <div className="post-data">
            <h2>Contenido del post</h2>
            <p>{content_post}</p>
        </div>
    </div>
  )
}

PostData.propTypes = {
    content_post:PropTypes.string.isRequired,
}

export default PostData