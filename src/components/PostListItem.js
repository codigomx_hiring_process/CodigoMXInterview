import React from 'react'
import PropTypes from 'prop-types'
import {Link} from 'react-router-dom'

const PostListItem = ({urlPath,post_id,post_content,responses_count,created_at}) => {
    return (
        <div>
            <div className="post-list-item">
                <div className="field">
                    <p>{post_content}</p>
                    <p>Numero de respuestas: {responses_count} </p>
                    <p>{created_at}</p>
                </div>
                <div className="field" style={{textAlign:'right'}} >
                    <Link to={`${urlPath}${post_id}/respuestas`}>Ver respuestas</Link>
                </div>
            </div>
        </div>
    );
};

PostListItem.propTypes = {
    post_content:PropTypes.string.isRequired,
    created_at:PropTypes.string.isRequired,
    post_id:PropTypes.number.isRequired,
    responses_count:PropTypes.number.isRequired,
};

export default PostListItem;