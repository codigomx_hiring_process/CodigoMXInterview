import React from 'react'
import PropTypes from 'prop-types'

const ResponseListItem = ({response_content,created_at}) => {
    return (
        <div>
            <div className="response-list-item">
                <div className="field">
                    <p>{response_content}</p>
                    <p>{created_at}</p>
                </div>
            </div>
        </div>
    );
};

ResponseListItem.propTypes = {
    response_content:PropTypes.string.isRequired,
    created_at:PropTypes.string.isRequired,
};

export default ResponseListItem;