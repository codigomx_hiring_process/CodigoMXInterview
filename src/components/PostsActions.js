import React from 'react';
import PropTypes from 'prop-types';

const PostsActions = ({children}) => {
    return (
        <div>
           <div className="posts-actions">
                <div>
                    {children}
                </div>
            </div> 
        </div>
    );
};

PostsActions.propTypes = {
    children:PropTypes.node.isRequired,
};

export default PostsActions;