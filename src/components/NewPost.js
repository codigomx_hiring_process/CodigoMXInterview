import React from 'react'
import { reduxForm, Field } from 'redux-form'
import PostsActions from './PostsActions';

const textAreaField = ({input,meta,label,name})=>(
    <div>
        <label htmlFor={name}>{label}</label>
        <br/>
        <input {...input} />
        <br/>
            {
                meta.touched && meta.error && <span> {meta.error} </span>
            }
    </div>
)

const isRequired = value => !value && "Este campo es requerido"

const NewPost = ({handleSubmit,submitting}) => {
    return (
        <div>
            <form onSubmit={handleSubmit}>
                <Field label="Contenido del post" name="post_content" component={textAreaField} type="text" validate={isRequired}></Field>
                <PostsActions>
                    <button type="submit" disabled={submitting} >Guardar</button>   
                </PostsActions>
            </form>
        </div>
    );
};



export default reduxForm({ form:"newPost" })(NewPost);