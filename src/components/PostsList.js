import React from 'react';
import PropTypes from 'prop-types';
import PostListItem from './PostListItem';

const PostsList = ({ posts }) => {
    return (
        <div>
            <div className="post-list">
                {
                    posts.map(post=>
                        (<PostListItem
                            key={post.post_id}
                            {...post}
                            urlPath="posts/"
                        />)
                    )
                }
            </div>
        </div>
    );
};

PostsList.propTypes = {
    posts:PropTypes.array.isRequired,
};

export default PostsList;