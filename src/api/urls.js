export const urlBase = 'http://localhost/api/'
export const urlPosts = `${urlBase}posts`
export const urlResponses = `${urlBase}posts/responses`