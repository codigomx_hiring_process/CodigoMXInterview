export const apiGet = (url,params=null) => async () => {
    if(params){
        let p = []
        for(var key in params){
            p.push(`${key}=${params[key]}`)
        }
        p = p.join('&')
        url += `?${p}`
    }
    const resolve = await fetch(url)
    return resolve.json()  
} 

export const apiPost = (url,data) => async () => {
    let formBody = [];
    for (var property in data) {
        let encodedKey = encodeURIComponent(property);
        let encodedValue = encodeURIComponent(data[property]);
        formBody.push(encodedKey + "=" + encodedValue);
    }
    formBody = formBody.join("&");

    const config = {
        method:'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        },
        body:formBody
    }
    
    const resolve   = await fetch(url,config)
    return resolve.json()
  }