import { combineReducers } from 'redux'
import { reducer as reduxForm } from 'redux-form'
import {posts} from './posts'
import {responses} from './responses'

export default combineReducers({
    posts,
    responses,
    form: reduxForm
})