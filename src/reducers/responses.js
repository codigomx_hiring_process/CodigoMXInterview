import { handleActions } from 'redux-actions'
import { FETCH_RESPONSES, INSERT_RESPONSE } from '../constans';

export const responses = handleActions(
    {
        [FETCH_RESPONSES]: (state,action) => [...action.payload],
        [INSERT_RESPONSE]: (state,action) => [...state,action.payload,]
    },[])