import { handleActions } from 'redux-actions'
import { FETCH_POSTS, INSERT_POST } from '../constans';

export const posts = handleActions(
    {
        [FETCH_POSTS]: (state,action) => [...action.payload],
        [INSERT_POST]: (state,action) => [action.payload,...state]
    },[])