import { createSelector } from 'reselect'

export const getPosts = state => state.posts

export const getPostById = createSelector(
    (state,props) => state.posts.find(c => c.post_id === parseInt(props.postId)),
    post => post
)